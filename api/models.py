from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class APIUser(AbstractUser):
    username = models.CharField(unique=True, max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(null=True, blank=True)
    last_request = models.DateTimeField(null=True, blank=True)

    def update_last_login(self):
        self.last_login = timezone.now()
        self.save()

    def update_last_request(self):
        self.last_request = timezone.now()
        self.save()

    class Meta:
        db_table = 'api_user'
        ordering = ['id']


class Post(models.Model):
    user = models.ForeignKey(to=APIUser, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    text = models.TextField()

    class Meta:
        db_table = 'api_post'
        ordering = ['id']


class Like(models.Model):
    user = models.ForeignKey(APIUser, related_name='likes', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'api_likes'
        unique_together = ('user', 'post')
        ordering = ['id']