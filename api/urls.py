from rest_framework import routers

from api.views.User import UserViewSet
from api.views.Post import PostViewSet
from api.views.Likes import LikeViewSet

router = routers.SimpleRouter()
router.register(r'user', UserViewSet)
router.register(r'post', PostViewSet)
router.register(r'likes', LikeViewSet)

urlpatterns = router.urls