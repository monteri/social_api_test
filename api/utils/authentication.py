import jwt
from django.conf import settings
from jwt.exceptions import DecodeError
from rest_framework import authentication
from rest_framework import exceptions

from api.models import APIUser
from api.utils.irc import IRC


class JsonWebTokenAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        if not token:
            return None

        parts = token.split()
        if len(parts) < 1:
            raise exceptions.AuthenticationFailed()

        try:
            payload = jwt.decode(parts[1], settings.JWT_SALT, algorithms=['HS256'])
            user = APIUser.objects.get(id=payload.get('id'))
            # Updating last request datetime
            user.update_last_request()
        except DecodeError:
            raise exceptions.AuthenticationFailed()
        except APIUser.DoesNotExist:
            raise exceptions.AuthenticationFailed(IRC['USER_NOT_FOUND']['ERROR_MESSAGE'])

        return (user, None)

