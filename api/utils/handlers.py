import logging
import traceback

from rest_framework import status
from rest_framework.exceptions import ValidationError, APIException
from rest_framework.response import Response
from rest_framework.views import exception_handler

from api.utils.irc import IRC


class ExceptionHandler(Exception):
    def __init__(self, error=None, status=400):
        self.status = status
        self.errors = dict()
        self.errors['errors'] = list()
        self.errors['errors'].append(error)

    def __str__(self):
        return '{} - {}'.format(self.status, str(self.errors))


logger = logging.getLogger('django.api.exception')


def create_error(error_message=IRC['INTERNAL_SERVER_ERROR']['ERROR_MESSAGE'], error_code=500):
    return {
        'ERROR_CODE': error_code,
        'ERROR_MESSAGE': error_message
    }


def add_error_for_lists_in_dict(dictionary, field=None):
    errors = []
    for key in dictionary:
        for list_value in dictionary[key]:
            title = f'{field} - {key}' if field else f'{key}'
            error_message = f'{title} - {list_value}'
            errors.append(create_error(error_message))

    return errors


def prepare_validation_error(exception_detail):
    errors = []
    for field, detail_item in exception_detail.items():
        if isinstance(detail_item, list):

            for detail_value in detail_item:

                if isinstance(detail_value, dict):
                    errors.extend(add_error_for_lists_in_dict(detail_value, field))

                else:
                    error_message = f'{field} - {detail_value}'
                    errors.append(create_error(error_message))

        elif isinstance(detail_item, dict):
            errors.extend(add_error_for_lists_in_dict(detail_item, field))

    return errors


def api_exception_handler(exc, context):
    if isinstance(exc, ExceptionHandler):
        return Response(exc.errors, status=exc.status)

    '''
    Go to the standard error response
    '''
    errors = []
    response = exception_handler(exc, context)
    if not response:
        response = Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    if isinstance(exc, APIException):

        if isinstance(exc, ValidationError):

            if isinstance(exc.detail, dict):
                errors.extend(prepare_validation_error(exc.detail))

            else:
                for detail_item in exc.detail:
                    errors.extend(add_error_for_lists_in_dict(detail_item))

        else:
            errors.append(create_error(exc.detail))

    else:
        errors.append(create_error(str(exc), response.status_code))

    response.data = {
        'errors': errors
    }
    traceback.print_exc()
    logger.error({
        'error': str(exc),
        'traceback': str(traceback.format_exc()),
        'request': str(context['request'].data)
    })
    return response