from config import settings

IRC = {
    "INTERNAL_SERVER_ERROR": {
        "ERROR_MESSAGE": "Internal Server Error.",
        "ERROR_CODE": 500
    },
    "USER_USERNAME_EXISTS": {
        "ERROR_MESSAGE": "Username exists.",
        "ERROR_CODE": 1001
    },
    "USER_CREDENTIALS_INVALID": {
        "ERROR_MESSAGE": "User invalid credentials.",
        "ERROR_CODE": 1002
    },
    "USER_EXISTS": {
        "ERROR_MESSAGE": "User with this username exists.",
        "ERROR_CODE": 1003
    },
    "WRONG_QUERY_PARAM_DATE_FORMAT": {
        "ERROR_MESSAGE": f"Given date has invalid format. Use {settings.QUERY_PARAM_DATE_FORMAT}",
        "ERROR_CODE": 1004
    },
}