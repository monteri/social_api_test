from django.db import transaction
from rest_framework.serializers import ModelSerializer, CharField, IntegerField, PrimaryKeyRelatedField

from api import models


class UserSerializer(ModelSerializer):
    id = IntegerField(read_only=True)
    username = CharField(allow_blank=False)
    password = CharField(write_only=True, trim_whitespace=False)

    class Meta:
        model = models.APIUser
        fields = ('id', 'password', 'username')

    @transaction.atomic
    def create(self, validated_data):
        return models.APIUser.objects.create_user(**validated_data)


class LikeSerializer(ModelSerializer):

    class Meta:
        model = models.Like
        fields = ('user', 'post', 'created_at')


class PostSerializer(ModelSerializer):
    user = PrimaryKeyRelatedField(read_only=True)
    like_count = IntegerField(read_only=True)

    class Meta:
        model = models.Post
        fields = ('id', 'user', 'text', 'like_count', 'created_at')
