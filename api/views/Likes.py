from datetime import datetime
import pytz

from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK

from api.models import Like
from api.utils.serializers import LikeSerializer
from api.utils.handlers import ExceptionHandler
from api.utils.irc import IRC
from config.settings import QUERY_PARAM_DATE_FORMAT


class LikeViewSet(ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [IsAuthenticated, ]

    @action(detail=False, methods=['get'], permission_classes=())
    def count(self, request):
        date_from = request.query_params.get('date_from')
        date_to = request.query_params.get('date_to')

        if date_from:
            date_from = parse_datetime(date_from)
        if date_to:
            date_to = parse_datetime(date_to)

        if date_from and date_to:
            likes_count = Like.objects.filter(created_at__gte=date_from, created_at__lte=date_to).count()
        elif date_from:
            likes_count = Like.objects.filter(created_at__gte=date_from).count()
        elif date_to:
            likes_count = Like.objects.filter(created_at__lte=date_to).count()
        else:
            likes_count = Like.objects.all().count()
        response = {'likes_count': likes_count}
        return Response(response, status=HTTP_200_OK)


def parse_datetime(string):
    try:
        date = datetime.strptime(string, QUERY_PARAM_DATE_FORMAT)
        return date.replace(tzinfo=pytz.UTC)
    except ValueError:
        raise ExceptionHandler(IRC['WRONG_QUERY_PARAM_DATE_FORMAT'])
