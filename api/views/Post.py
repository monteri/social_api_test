from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK

from api.models import Post, Like
from api.utils.serializers import PostSerializer


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated, ]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    @action(detail=True, methods=['post'], permission_classes=(IsAuthenticated, ))
    def like(self, request, pk=None):
        post = self.get_object()

        # Adds like to post if doesn't exist. Deletes like if exists.
        if not Like.objects.filter(post=post, user=request.user).exists():
            Like.objects.create(post=post, user=request.user)
            return Response(status=HTTP_201_CREATED)
        else:
            Like.objects.filter(post=post, user=request.user).delete()
            return Response(status=HTTP_200_OK)



