import jwt

from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_403_FORBIDDEN, HTTP_200_OK
from rest_framework.viewsets import GenericViewSet

from api.models import APIUser
from api.utils.handlers import ExceptionHandler
from api.utils.irc import IRC
from api.utils.serializers import UserSerializer


class UserViewSet(GenericViewSet):
    queryset = APIUser.objects.all()
    serializer_class = UserSerializer

    @action(detail=False, methods=['post'], permission_classes=())
    @transaction.atomic
    def register(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        validate_password(data['password'])
        if self.get_queryset().filter(username__iexact=data['username']).exists():
            raise ExceptionHandler(IRC['USER_EXISTS'])

        serializer.save(username=data['username'])
        user = serializer.instance
        user.save()
        return Response(status=HTTP_201_CREATED)

    @action(detail=False, methods=['post'], permission_classes=())
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        username = data['username']
        try:
            user = self.get_queryset().get(username__iexact=username)
            # Updating last login datetime
            user.update_last_login()
        except APIUser.DoesNotExist:
            raise ExceptionHandler(IRC['USER_CREDENTIALS_INVALID'], status=HTTP_403_FORBIDDEN)
        if not user.check_password(data['password']):
            raise ExceptionHandler(IRC['USER_CREDENTIALS_INVALID'], status=HTTP_403_FORBIDDEN)
        token = jwt.encode({'id': user.id}, settings.JWT_SALT, algorithm='HS256')
        return Response({'token': token.decode("utf-8")})

    @action(detail=False, methods=['get'], permission_classes=())
    def activity(self, request):
        response = []
        for q in self.queryset:
            response.append({
                'id': q.id,
                'username': q.username,
                'last_login': q.last_login,
                'last_request': q.last_request
            })
        return Response(response, status=HTTP_200_OK)
