Installation for django:  
1. Create virtualenv  
2. pip install -r requirements.txt  
3. python manage.py migrate  
4. python manage.py runserver  

For script:  
1. Configuration file: script_config.json  
2. python script.py  