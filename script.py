import json
import string
import random
import requests


class Bot:
    base_url = 'http://localhost:8000'

    def __init__(self, filename):
        with open(filename) as json_file:
            data = json.load(json_file)
            self.number_of_users = data['number_of_users']
            self.max_post_per_user = data['max_post_per_user']
            self.max_likes_per_user = data['max_likes_per_user']

    def run(self):
        print("number_of_users: " + str(self.number_of_users))
        print("max_post_per_user: " + str(self.max_post_per_user))
        print("max_likes_per_user " + str(self.max_likes_per_user))
        tokens = []
        for i in range(self.number_of_users):
            print("--------------------------------")
            username, password = self.generate_credentials()
            print("username: " + username)
            print("password: " + password)

            signed_up_status = self.sign_up_user(username, password)
            print("sign_up_status: " + str(signed_up_status))

            token = self.log_in_user(username, password)
            print("token: " + token)
            tokens.append(token)

            created_posts = self.create_posts(token)
            print("number of created posts: " + str(len(created_posts)))
            print("list of created posts: " + str(created_posts))

        for token in tokens:
            print("--------------------------------")
            liked_posts = self.like_posts(token)
            print("number of liked posts: " + str(len(liked_posts)))
            print("liked posts: " + str(liked_posts))

        print("--------------------------------")
        user_activity = self.get_user_activity()
        pretty_user_activity = json.dumps(user_activity, indent=4)
        print("user activity: " + pretty_user_activity)
        
        likes_count = self.get_likes_count()
        print("--------------------------------")
        print(likes_count)

    def generate_credentials(self, length=6):
        credentials = ''.join(random.choices(string.ascii_lowercase + string.digits, k=length))
        return credentials, credentials

    def sign_up_user(self, username, password):
        url = self.base_url + '/api/v1/user/register/'
        data = {'username': f'{username}', 'password': f'{password}'}
        response = requests.post(url=url, data=data)
        return response.status_code

    def log_in_user(self, username, password):
        url = self.base_url + '/api/v1/user/login/'
        data = {'username': f'{username}', 'password': f'{password}'}
        response = requests.post(url=url, data=data)
        return response.json()['token']

    def create_posts(self, token):
        url = self.base_url + '/api/v1/post/'
        header = self._get_header(token)
        post_list = []
        # Generate number from 1 to max_post_per_user
        for i in range(0, random.randint(1, self.max_post_per_user)):
            # Random text for post
            data = {'text': ''.join(random.choices(string.ascii_uppercase, k=6))}
            response = requests.post(url=url, data=data, headers=header)
            post_list.append(response.json())
        return post_list

    def like_posts(self, token):
        id_list = self.get_post_ids(token)
        header = self._get_header(token)
        liked_posts = []
        for i in range(0, random.randint(1, self.max_likes_per_user)):
            random_index = random.randrange(len(id_list))
            post_id = id_list.pop(random_index)
            url = self.base_url + f"/api/v1/post/{post_id}/like/"
            response = requests.post(url=url, headers=header)
            liked_posts.append({
                'post_id': post_id,
                'status': response.status_code
            })
        return liked_posts

    def get_post_ids(self, token):
        post_list = self.get_posts(token)
        id_list = []
        for post in post_list:
            id_list.append(post['id'])
        return id_list

    def get_posts(self, token):
        url = self.base_url + '/api/v1/post/'
        header = self._get_header(token)
        response = requests.get(url=url, headers=header)
        return response.json()

    def get_user_activity(self):
        url = self.base_url + '/api/v1/user/activity/'
        response = requests.get(url=url)
        return response.json()

    def get_likes_count(self):
        url = self.base_url + '/api/v1/likes/count/'
        response = requests.get(url=url)
        return response.json()

    def _get_header(self, token):
        return {'Authorization': f'Token {token}'}


def main():
    bot = Bot('script_config.json')
    bot.run()


if __name__ == '__main__':
    main()
